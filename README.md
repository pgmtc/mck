# mck
[![pipeline status](https://gitlab.com/pgmtc/mck/badges/master/pipeline.svg)](https://gitlab.com/pgmtc/mck/commits/master)
[![coverage report](https://gitlab.com/pgmtc/mck/badges/master/coverage.svg)](https://gitlab.com/pgmtc/mck/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/pgmtc/mck)](https://goreportcard.com/report/gitlab.com/pgmtc/mck)
[![Documentation](https://godoc.org/gitlab.com/pgmtc/mck?status.svg)](http://godoc.org/gitlab.com/pgmtc/mck)

CLI tool for mocking up external dependency of micro services / containers. Mck creates stand alone servers / containers which are accessed over the network, the same way as in the reality.

The tool is provided the script of sequential steps which are then returned in the same sequence when the server is hit with HTTP request.
Content of the step supports both string and file content (response can be loaded from external file)
#### Installation:
##### Mac OS
```bash
brew tap pgmtc/repo
brew install mck
```
#### Linux, Windows
* go to https://gitlab.com/pgmtc/mck/pipelines
* download build artefacts (cloud icon in the right)
* extract and put relevant binary somewhere to your PATH


#### Example: 
* create a file called mck.yml with the following contents
```yaml
ignores:
  - /favicon.ico

steps:
- contenttype: text/plain
  statuscode: 200
  content: Plain text response

- contenttype: text/xml
  statuscode: 200
  content: <data><element1>value1</element1></data>

- contenttype: application/json
  statuscode: 200
  content: "{ \"data\": { \"element1\": \"value1\" } }"
  
- contenttype: text/plain
  statuscode: 500
  content: some internal server error - go whistle
```
* From the same directory, run the following
`mck`

* Point your browser to http://localhost:8080/ and keep refreshing. Responses should be following the same order ad the script

#### Content from the files
If the desired content to return is too big or it is inconvenient to define it on one line, you can provide file name instead
```yaml
- contenttype: application/json
  statuscode: 200
  content: some-file.json
```

#### Delaying response
If you for some reason need to delay response, simply add delayms attribute
```yaml
- contenttype: text/plain
  statuscode: 200
  content: some response
  delayms: 200
```

#### Supported arguments
```
  -f string
        file path to screenplay yaml (default "mck.yaml")
  -p int
        server port to listen on (default 8080)
  -w    run forever (restart after last step)
```
#### To Do:
* support of cookies
* support of http headers
* distinguish among paths (each of the path could have its own sequence)
* provide control API - ways to control responses interactively during the runtime as opposed to being loaded when the tool starts
* recorder - provide pass through proxy to a real service and allow to record the real responses and store them in a script
