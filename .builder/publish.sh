#!/usr/bin/env bash
set -e
PKG="$1"
VERSION="$2"
PKG_PATH="dist/$PKG"
SHA=$(sha256sum ${PKG_PATH} | awk '{ print $1 }')
S3_PATH="pgmtc-releases/mck/"
S3_PUBLIC_PATH="https://s3.eu-west-2.amazonaws.com/${S3_PATH}${PKG}"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
BREW_GIT="git@github.com:pgmtc/homebrew-repo.git"
BREW_PATH="${SCRIPT_DIR}/.brew-repo"

#aws s3 cp ${PKG_PATH} s3://${S3_PATH}
#echo "Release available at: ${S3_PUBLIC_PATH}"

echo "Cloning homebrew repo"
rm -rf ${BREW_PATH}
git clone ${BREW_GIT} ${BREW_PATH}

echo "Generating formula"

cat <<EOT > ${BREW_PATH}/Formula/mck.rb
class Mck < Formula
  desc "mck - cli tool for mocking external dependencies and microservices"
  homepage "https://gitlab.com/pgmtc/mck"
  url "${S3_PUBLIC_PATH}"
  version "${VERSION}"
  sha256 "${SHA}"

  def install
    bin.install "mck"
  end
end
EOT

cd ${BREW_PATH}
git commit -a -m "release of ${VERSION}"
git push
