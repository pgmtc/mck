package main

import (
	"flag"
	"fmt"
	"gitlab.com/pgmtc/mck/screenplay"
	"gitlab.com/pgmtc/mck/server"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"os/user"
	"strings"
)

func main() {
	scriptFile := flag.String("f", "mck.yaml", "file path to screenplay yaml")
	port := flag.Int("p", 8080, "server port to listen on")
	forever := flag.Bool("w", false, "run forever (restart after last step)")
	flag.Parse()

	if !fileExists(*scriptFile) {
		fmt.Printf("Cannot open the script file: %s\nCreate it, or provide some other one with -f parameter\nUsage:\n", *scriptFile)
		flag.PrintDefaults()
		os.Exit(1)
	}
	sp := screenplay.CreateSequential()
	unmarshalYaml(*scriptFile, sp)

	ctrl := server.CreateController(&server.MockHTTPServer{Port: *port}, sp, *forever)
	ctrl.StartServer()
}

func fileExists(path string) bool {
	parsedPath := parsePath(path)
	if _, err := os.Stat(parsedPath); err == nil {
		return true
	}
	return false
}

func parsePath(path string) (result string) {
	usr, _ := user.Current()
	if path == "." {
		path, _ = os.Getwd()
	}
	result = strings.Replace(path, "~", usr.HomeDir, 1)

	if !strings.HasPrefix(result, "/") {
		currentDir, _ := os.Getwd()
		result = currentDir + "/" + result
	}
	return
}

func marshalYaml(data interface{}, fileName string) (resultErr error) {
	bytes, _ := yaml.Marshal(data)
	if err := ioutil.WriteFile(fileName, bytes, 0644); err != nil {
		resultErr = fmt.Errorf("error writing file: %s", err.Error())
		return
	}

	return
}

func unmarshalYaml(fileName string, out interface{}) (resultErr error) {
	bytes, err := ioutil.ReadFile(fileName)
	if err != nil {
		resultErr = fmt.Errorf("error when opening file %s: %s", fileName, err.Error())
		return
	}

	if err := yaml.Unmarshal(bytes, out); err != nil {
		resultErr = fmt.Errorf("error when unmarshalling file %s: %s", fileName, err.Error())
		return
	}
	return
}
