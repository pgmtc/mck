package screenplay

import "gitlab.com/pgmtc/mck/common"

// ScreenPlay controls the order of steps used by the server
type ScreenPlay interface {
	Connect(controlChannel chan common.Event)
	Ignore(path string)
	Add(item screenplayStep)
	Get(path string) (RuntimeStep, error)
	Eval()
	Reset()
}

// Context represents runtime context for the step
type Context struct {
	RandString string
	ServerName string
}
