package screenplay

import (
	"gitlab.com/pgmtc/mck/common"
	"reflect"
	"testing"
)

func TestCreateSequential(t *testing.T) {
	sequential := CreateSequential()
	if sequential == nil {
		t.Errorf("Expected not to be null")
	}
}

func Test_sequential_Comms(t *testing.T) {
	// TODO improve this
	s := CreateSequential()
	ch := make(chan common.Event)
	s.Connect(ch)
	go s.Eval()
	instruction := <-ch
	if instruction != common.ScreenplayEnd {
		t.Errorf("Expected message %d, got %d", common.ScreenplayEnd, instruction)
	}
}

func Test_sequential_Ignore(t *testing.T) {
	s := CreateSequential()
	s.Add(screenplayStep{
		Description: "step 1",
		Content:     "content 1",
	})
	s.Ignore("favicon.ico")
	step, _ := s.Get("favicon.ico")
	if !reflect.DeepEqual(step, _ItemIgnore) {
		t.Errorf("Expected ignore item to be returned")
	}
	step, _ = s.Get("/")
	if reflect.DeepEqual(step, _ItemIgnore) {
		t.Errorf("Not expected ignore item to be returned")
	}
}

func Test_sequential_Reset(t *testing.T) {
	s := CreateSequential()
	step1 := screenplayStep{Description: "step1", Content: "content1"}
	step2 := screenplayStep{Description: "step2", Content: "content2"}
	step3 := screenplayStep{Description: "step3", Content: "content3"}
	s.Add(step1)
	s.Add(step2)
	s.Add(step3)

	result, err := s.Get("/")
	if err != nil {
		t.Errorf("Unexpected error, got %s", err.Error())
	}
	if result.Description != step1.Description {
		t.Errorf("Expected %s to match %s", result.Description, step1.Description)
	}

	result, err = s.Get("/")
	if err != nil {
		t.Errorf("Unexpected error, got %s", err.Error())
	}
	if result.Description != step2.Description {
		t.Errorf("Expected %s to match %s", result.Description, step2.Description)
	}

	s.Reset()
	result, err = s.Get("/")
	if err != nil {
		t.Errorf("Unexpected error, got %s", err.Error())
	}
	if result.Description != step1.Description {
		t.Errorf("Expected %s to match %s", result.Description, step1.Description)
	}
}

func Test_sequential_current(t *testing.T) {
	s := CreateSequential()
	step1 := screenplayStep{Description: "step1", Content: "content1"}
	s.Add(step1)
	result, err := s.Get("/")
	if err != nil {
		t.Errorf("Unexpected error, got %s", err.Error())
	}
	if result.Description != step1.Description {
		t.Errorf("Expected %s to match %s", result.Description, step1.Description)
	}

	_, err = s.Get("/")
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}

}
