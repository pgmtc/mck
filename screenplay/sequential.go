package screenplay

import (
	"fmt"
	"gitlab.com/pgmtc/mck/common"
)

type sequential struct {
	currentStep    int
	Steps          []screenplayStep
	Ignores        []string
	controlChannel chan common.Event
}

// CreateSequential is a 'constructor' for sequential struct
func CreateSequential() ScreenPlay {
	sp := sequential{}
	return &sp
}

func (s *sequential) Eval() {
	stepsLeft := len(s.Steps) - s.currentStep
	if stepsLeft == 0 {
		fmt.Println("restarting")
		s.controlChannel <- common.DoReset
	}
}

func (s *sequential) Connect(controlChannel chan common.Event) {
	s.controlChannel = controlChannel
}

func (s *sequential) Ignore(path string) {
	s.Ignores = append(s.Ignores, path)
}

func (s *sequential) Add(item screenplayStep) {
	s.Steps = append(s.Steps, item)
}

func (s *sequential) Get(path string) (item RuntimeStep, resultErr error) {
	if arrContains(s.Ignores, path) {
		item = _ItemIgnore
		return
	}
	nextStep, resultErr := s.next()
	if resultErr != nil {
		return
	}
	item = CreateRuntimeStep(nextStep, Context{})
	return
}

func (s *sequential) next() (item screenplayStep, err error) {
	item, err = s.current()
	if err == nil {
		s.currentStep++
	}
	return
}

func (s *sequential) current() (item screenplayStep, resultErr error) {
	if s.currentStep >= len(s.Steps) {
		resultErr = fmt.Errorf("no recorded steps left")
		return
	}
	item = s.Steps[s.currentStep]
	return
}

func (s *sequential) Reset() {
	s.currentStep = 0
}

func arrContains(arr []string, value string) bool {
	for _, element := range arr {
		if element == value {
			return true
		}
	}
	return false
}
