package screenplay

import (
	"bytes"
	"html/template"
	"io/ioutil"
	"os"
	"os/user"
	"strings"
)

// RuntimeStep is generated from step and is used by the server to send the response
type RuntimeStep struct {
	ContentType string
	StatusCode  int
	Description string
	DelayMs     int
	Body        []byte
}

// CreateRuntimeStep is used as a 'constructor' for RuntimeStep
func CreateRuntimeStep(step screenplayStep, context Context) RuntimeStep {
	return RuntimeStep{
		ContentType: step.ContentType,
		StatusCode:  step.StatusCode,
		Description: step.Description,
		Body:        processBody(step.Content, context),
		DelayMs:     step.DelayMs,
	}
}

func processBody(content string, context Context) []byte {
	var resultString string
	resultString = processFile(content)
	resultString = processContext(resultString, context)
	return []byte(resultString)
}

func processFile(content string) string {
	if fileExists(content) {
		if bytes, err := ioutil.ReadFile(parsePath(content)); err == nil {
			return string(bytes)
		}
	}
	return content
}

func processContext(content string, context Context) string {
	t := template.Must(template.New("spitem").Parse(content))
	wr := bytes.Buffer{}
	err := t.Execute(&wr, context)
	if err != nil {
		return content
	}
	return wr.String()
}

var _ItemIgnore = RuntimeStep{
	StatusCode: 404,
	Body:       []byte("ignored"),
}

func fileExists(path string) bool {
	parsedPath := parsePath(path)
	if _, err := os.Stat(parsedPath); err == nil {
		return true
	}
	return false
}

func parsePath(path string) (result string) {
	usr, _ := user.Current()
	if path == "." {
		path, _ = os.Getwd()
	}
	result = strings.Replace(path, "~", usr.HomeDir, 1)

	if !strings.HasPrefix(result, "/") {
		currentDir, _ := os.Getwd()
		result = currentDir + "/" + result
	}
	return
}
