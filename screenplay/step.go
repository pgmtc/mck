package screenplay

type screenplayStep struct {
	ContentType string
	StatusCode  int
	Content     string
	DelayMs     int
	Description string
}
