package screenplay

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"testing"
)

func TestCreateRuntimeStep(t *testing.T) {
	step := screenplayStep{
		StatusCode:  200,
		ContentType: "text/plain",
		Description: "step description",
		Content:     "step content",
	}

	rtStep := CreateRuntimeStep(step, Context{})
	if rtStep.StatusCode != step.StatusCode {
		t.Errorf("Expected %d to equal %d", rtStep.StatusCode, step.StatusCode)
	}
	if rtStep.ContentType != step.ContentType {
		t.Errorf("Expected %s to equal %s", rtStep.ContentType, step.ContentType)
	}
	if rtStep.Description != step.Description {
		t.Errorf("Expected %s to equal %s", rtStep.Description, step.Description)
	}
	if string(rtStep.Body) != step.Content {
		t.Errorf("Expected %s to equal %s", string(rtStep.Body), step.Content)
	}
	if rtStep.DelayMs != step.DelayMs {
		t.Errorf("Expected %d to equal %d", rtStep.DelayMs, step.DelayMs)
	}

}

func Test_ProcessBody(t *testing.T) {
	tmpFile := mkTempFile("test.xml", "<xml>content</xml>")
	defer os.RemoveAll(tmpFile)
	type args struct {
		bytes []byte
	}
	tests := []struct {
		name string
		in   string
		want []byte
	}{
		{
			name: "empty test",
			in:   "some content",
			want: []byte("some content"),
		},
		{
			name: "empty test",
			in:   tmpFile,
			want: []byte("<xml>content</xml>"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := processBody(tt.in, Context{}); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("processBody() = %v, want %v", string(got), string(tt.want))
			}
		})
	}
}

func Test_processContext(t *testing.T) {
	ctx := Context{
		RandString: "A1234",
		ServerName: "ServerName",
	}

	content := "Test {{.RandString}} result"
	result := processContext(content, ctx)
	if result != "Test A1234 result" {
		t.Errorf("Expected '%s', got '%s'", ctx.RandString, result)
	}
}

func mkTempFile(fileName string, fileContents string) string {
	tmpDir := mkTempDir("")
	filePath := filepath.Join(tmpDir, fileName)
	return mkFile(filePath, fileContents)
}

func mkTempDir(dirSuffix string) string {
	tmpDir, _ := ioutil.TempDir("", dirSuffix)
	return tmpDir
}

func mkFile(path string, fileContents string) string {
	byteContent := []byte(fileContents)
	filePath := parsePath(path)
	ioutil.WriteFile(filePath, byteContent, 0644)
	return filePath
}
