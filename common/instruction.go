package common

// Event represents set of events used for cross channel communication
type Event int

const (
	// ScreenplayEnd indicates that the Screenplay had ended
	ScreenplayEnd Event = iota // 0
	// DoReset asks for requesting Screenplay reset
	DoReset
	// ServerStop Indicates that the server had stopped
	ServerStop
)
