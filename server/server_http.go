package server

import (
	"context"
	"fmt"
	"gitlab.com/pgmtc/mck/common"
	"gitlab.com/pgmtc/mck/screenplay"
	"net/http"
	"strconv"
	"time"
)

// MockHTTPServer is a mock server serving over HTTP
type MockHTTPServer struct {
	Port           int
	server         *http.Server
	debugChannel   chan string
	controlChannel chan common.Event
	screenPlay     screenplay.ScreenPlay
	block          bool
}

// Stop shuts server down
func (s *MockHTTPServer) Stop() {
	s.block = true
	s.debugChannel <- "shutting down the http server"
	ctx, err := context.WithTimeout(context.Background(), 5*time.Second)
	if err != nil {
		s.controlChannel <- common.ServerStop
	}
	if err := s.server.Shutdown(ctx); err != nil {
		s.controlChannel <- common.ServerStop
		s.debugChannel <- err.Error()
	}
}

// Start spin server up
func (s *MockHTTPServer) Start(screenPlay screenplay.ScreenPlay, controlChannel chan common.Event, debugChannel chan string) {
	s.debugChannel = debugChannel
	s.controlChannel = controlChannel
	s.screenPlay = screenPlay
	http.HandleFunc("/", s.defaultHandler)
	s.server = &http.Server{
		Addr: ":" + strconv.Itoa(s.Port),
	}

	go func() {
		if err := s.server.ListenAndServe(); err != nil {
			s.debugChannel <- err.Error()
		}
		s.controlChannel <- common.ServerStop
	}()
	s.debugChannel <- "server listening on :" + strconv.Itoa(s.Port)
}

func (s *MockHTTPServer) defaultHandler(w http.ResponseWriter, req *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Access-Control-Allow-Methods", "*")
	w.Header().Add("Access-Control-Allow-Headers", "*")
	if req.Method == http.MethodOptions {
		s.debugChannel <- fmt.Sprintf("ignoring OPTIONS request")
		return
	}
	if !s.block {
		defer s.screenPlay.Eval()
		step, err := s.screenPlay.Get(req.RequestURI)
		if err != nil {
			s.debugChannel <- fmt.Sprintf("%s '%s' -> %s", req.Method, req.RequestURI, err.Error())
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
			return
		}
		s.debugChannel <- fmt.Sprintf("%s '%s' -> %s (delay %dms)", req.Method, req.RequestURI, step.Description, step.DelayMs)
		w.Header().Add("Content-Type", step.ContentType)
		w.WriteHeader(step.StatusCode)
		w.Write(step.Body)
		if step.DelayMs > 0 {
			time.Sleep(time.Duration(step.DelayMs) * time.Millisecond)
		}
	}
}
