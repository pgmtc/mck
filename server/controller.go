package server

import (
	"fmt"
	"gitlab.com/pgmtc/mck/common"
	"gitlab.com/pgmtc/mck/screenplay"
	"os"
)

// SequentialController represents controller with steps run in a sequence
type SequentialController struct {
	server         MockServer
	screenPlay     screenplay.ScreenPlay
	debugChannel   chan string
	controlChannel chan common.Event
	runForever     bool
}

// CreateController is a 'constructor' for SequentialController struct
func CreateController(server MockServer, sp screenplay.ScreenPlay, runForever bool) *SequentialController {
	c := SequentialController{
		server:         server,
		debugChannel:   make(chan string),
		controlChannel: make(chan common.Event),
		screenPlay:     sp,
		runForever:     runForever,
	}
	sp.Connect(c.controlChannel)

	return &c
}

// StartServer instruct controller to start mock server
func (c *SequentialController) StartServer() {
	go c.server.Start(c.screenPlay, c.controlChannel, c.debugChannel)
	go c.listenDebug()
	c.listenControl()
}

func (c *SequentialController) listenDebug() {
	for {
		msg := <-c.debugChannel
		fmt.Printf("%s\n", msg)
	}
}

func (c *SequentialController) listenControl() {
	for {
		instruction := <-c.controlChannel
		switch instruction {
		//case common.SCREENPLAY_STEP:
		case common.ScreenplayEnd:
			if !c.runForever {
				c.server.Stop()
			} else {
				c.screenPlay.Reset()
			}
		case common.DoReset:
			c.screenPlay.Reset()
		case common.ServerStop:
			c.debugChannel <- "exiting"
			os.Exit(0)
			return
		}
	}
}
