package server

import (
	"gitlab.com/pgmtc/mck/common"
	"gitlab.com/pgmtc/mck/screenplay"
)

// MockServer is an interface for server used by the controller
//go:generate mockgen -destination=./mocks/mock_server.go -package=mocks mck/server MockServer
type MockServer interface {
	Start(screenPlay screenplay.ScreenPlay, controlChannel chan common.Event, debugChannel chan string)
	Stop()
}
